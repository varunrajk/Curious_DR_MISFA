
__author__ = 'Varun Kompella, varun@idsia.ch'

"""
Curiosity Driven Modular Increment Slow Feature Analysis (Curious Dr. MISFA) Python Implementation

Extract slow feature abstractions from a set of time-varying input streams incrementally, 
in the order of increasing learning difficulty. More information about Curious Dr. MISFA
can be found in V.R. Kompella,  Slowness Learning for Curiosity Driven Agents, 
PhD Thesis, 2015.

"""

import copy
import numpy as np
from scipy.linalg import norm
from Abstraction_Estimator import (IncSFANode, IncSFA2Node, ROCClusteringNode)
from Reinforcement_Learner import LSPINode
from misc import loadfile, rouletteWheel


class CuriousDrMISFANode(object):
    """
    Curiosity Driven Modular Increment Slow Feature Analysis (Curious Dr. MISFA) Python Implementation
    
    Extract slow feature abstractions from a set of time-varying input streams incrementally, 
    in the order of increasing learning difficulty. More information about Curious Dr. MISFA
    can be found in V.R. Kompella,  Slowness Learning for Curiosity Driven Agents, 
    PhD Thesis, 2015.

    **Inputs**
    
      ``inputSrcs``
          A set of time-varying input sources (each input source is a class of type inputSrc) 
    
      ``args``
          A dictionary of arguments for the algorithm 
          
      ``adaptiveModuleArgs``
          A dictionary of specific arguments for the adaptive IncSFA-ROC module

      ``gatingSysArgs``
          A dictionary of specific arguments for the gating system

    **Additionally Defined Class Structures**
    
      ``AgentEnvironment``
          The internal environment for the RL agent in Curious Dr. MISFA
          
      ``AdaptiveModule``
          IncSFA-ROC adaptive abstraction module
          
      ``GatingSys``
          Gating system
          
      ``RewardModel``
          Updating intrinsic reward function model
    
    """
    
    
    def __init__(self, inputSrcs, args, adaptiveModuleArgs, gatingSysArgs):
        self.inputSrcs = inputSrcs
        numSrcs = len(inputSrcs)
        self.args = args
        self.tau = self.args['tau']
        # Decay Parameter for epsilon-greedy
        self.decay = self.args['decay']
        # Number of stochastic vs deterministic execution cycles
        self.dtCnt = [0, 0]
        # AgentEnv 
        self.agentEnv = AgentEnvironment(numSrcs, delta=self.args['delta'], sigma=self.args['sigma'], beta=self.args['beta'], L=self.args['L'])
        # Adaptive Module init
        self.adapModule = AdaptiveModule(*adaptiveModuleArgs)
        # Gating System Init
        self.gatingSys = GatingSystem(*gatingSysArgs)
        # Reward Model
        self.rModel = RewardModel(numSrcs, N=self.args['rlN'])
        # Behaviour RL
        self.RLArgs = self.args['RLArgs']
        self.RL = LSPINode(numActions=2, stateLims=self.RLArgs['stateLims'], basis=self.RLArgs['basis'], 
                            gamma=self.RLArgs['gamma'], R=self.rModel.R, **self.RLArgs['args'])
        self.RL.genSimulatedSamples()
        self.eps = self.args['eps']
        # Target RL        
        self.dRLArgs = self.args['dRLArgs']
        self.dRL = LSPINode(numActions=self.dRLArgs['numActions'], stateLims=self.dRLArgs['stateLims'], basis=self.dRLArgs['basis'], 
                            gamma=self.dRLArgs['gamma'], R=self.rModel.Rt, **self.dRLArgs['args'])
            
        self.dRL.genSimulatedSamples()
        self.nu = self.args['nu']
        # Transitions Models
        self.P = [self.RLArgs['args']['P'], self.dRLArgs['args']['P']]
        if self.RLArgs['args']['P'] == 'int':
            self.P[0] = self.RL.basisfn.gp.P
        if self.dRLArgs['args']['P'] == 'int':
            self.P[1] = self.dRL.basisfn.gp.P
        # Algorithm iteration count
        self.cnt = 0
        # Hist Variables
        self.incSFAOutput = []
        self.configData = []
        

    def getAction(self, s):
        ba = np.asscalar(self.RL.pi(s))
        ta = np.asscalar(self.dRL.pi(s))
        if (np.random.random() < self.eps):
            a = [0,np.random.randint(0,2)]
        else:
            if (np.random.random() < self.nu):
                a = [0,ba]
                self.dtCnt[0]+=1
            else:    
                a = [1,ta]
                self.dtCnt[1]+=1
        return [int(i) for i in a]

    # def updateEpsGreedy(self, r):
    #     if r > 0.0:
    #         self.eps *= self.decay
    #         self.nu *= self.decay
    #     if self.eps < 0.9:
    #         self.eps *= 0.992
    #         self.nu *= 0.992
    #     if self.eps < 0.6:
    #         self.eps *= 0.99
    #         self.nu *= 0.99

    def updateEpsGreedy(self, r):
        # if r > 0.0:
        self.eps *= self.decay
            # self.nu *= self.decay

        ##iCub experiment
        # if self.eps < 0.97:
        #     self.eps *= 0.

        #signals experiment
        if self.eps < 0.8:
            self.eps *= 0.95

    def update(self):
        
        #observe internal state
        sint = self.agentEnv.getObservation()
        
        #get internal action and its handle (0:behavior-policy or 1:target-policy)
        [aint_h, aint] = self.getAction(sint)

        #perform internal action        
        self.agentEnv.performAction(self.P[aint_h], aint)
        
        #observe next internal state
        sint_ = self.agentEnv.getObservation()

        #action to stay-switch when selected from target-policy
        if aint_h == 1:
            if sint_== sint: aint = 0
            else: aint = 1

        #assign the adaptiveModule's source to the new input source 
        self.adapModule.inputSrc = self.inputSrcs[sint_]    

        #collect observations from the new input source
        inputData, configData, rocClusIndx = self.adapModule.getObservations(self.tau)

        #generate gating signal         
        [gatingSig, encodedOutput] = self.gatingSys.genGatingSignal(inputData,rocClusIndx)

        if gatingSig:
            #update adaptive module
            incsfaout = self.adapModule.updateAdaptiveModule(inputData,rocClusIndx)
            #get intrinsic reward
            r = self.agentEnv.getReward(self.adapModule)
            #save hist variables for plotting
            self.incSFAOutput = incsfaout
            self.configData = configData 
        else:
            #no reward
            r = 0.0
            #save hist variables for plotting
            self.incSFAOutput = encodedOutput
            self.configData = configData 

        # Update the epsilon greedy params
        self.updateEpsGreedy(r)
        
        #Update the reward Model
        self.rModel.update([sint,aint,sint_,r])

        self.intObservation = [sint,aint_h,aint,sint_,r]

        # update RLs
        self.RL.update(iterval=1, verbose=False)
        self.dRL.update(iterval=1, verbose=False)

        self.cnt += 1

    def monitorVariables(self, buf=None):
        if buf is not None:
            if 'intObservation' in buf.keys():
                buf['intObservation'].append(self.intObservation)
            if 'incSFAOutput' in buf.keys():
                buf['incSFAOutput'].append(self.incSFAOutput)
            if 'configData' in buf.keys():
                buf['configData'].append(self.configData)
            if 'rocEstError' in buf.keys():
                buf['rocEstError'].extend(self.adapModule.progError)
            if 'rewardFunction1' in buf.keys():
                buf['rewardFunction1'].append(self.rModel.R1.copy())
            if 'rewardFunction2' in buf.keys():
                buf['rewardFunction2'].append(self.rModel.rewM.copy())
            if 'intPolicy' in buf.keys():
                buf['intPolicy'].append(copy.deepcopy(self.RL._Pi))
            if 'intdPolicy' in buf.keys():
                buf['intdPolicy'].append(copy.deepcopy(self.dRL._Pi))
        return buf


    def createNewModule(self):
        self.eps = self.args['eps']
        self.nu = self.args['nu']
        self.dtCnt = [0, 0]
        self.adapModule.createNewModule()    
        self.RL.reset()
        self.rModel.reset()

    def saveCurrModule(self):
        self.gatingSys.saveCurrModule(self.adapModule.node)



#######################################################################################################
###############################        Additional Defined Classes        ##############################
#######################################################################################################


class AgentEnvironment(object):
    def __init__(self, numSrcs, delta=0.3, sigma=5., beta=1., L=200):
        self.numSrcs = numSrcs
        self.St = np.random.randint(numSrcs)
        self.At = np.random.randint(2)

        self.epilenMean = np.zeros(self.numSrcs, dtype='float')
        self.epicnt = np.zeros(self.numSrcs)
        
        self.delta = delta
        self.sigma = sigma
        self.beta = beta
        self.L = L
        self.cnt = 0
        
    def performAction(self, P, a):
        self.At = a
        self.St = rouletteWheel(P[:,a,:][self.St])
        
    def getReward(self, ul):
        [estErr, dEstErr] = ul.getProgressError()
        #Curiosity Reward
        curReward = 0.0
        #Expert Reward
        expReward = 0.0
        for i in xrange(len(dEstErr)):
            ir = dEstErr[i]
            if ir > 0.0:
                ir = -np.abs(ir)
            else:
                ir = np.abs(ir)
            curReward += ir
            curReward = np.clip(curReward,-self.L,self.L)
            expReward += np.exp(-(estErr[i]-self.delta)**2/(2*self.sigma**2))
        cumReward = curReward + self.beta*expReward
        self.cnt+=1
        return cumReward

    def getObservation(self):
        return self.St

    def reset(self):
        self.St = np.random.randint(self.numSrcs)
        self.At = np.random.randint(2)

        self.epilenMean = np.zeros(self.numSrcs, dtype='float')
        self.epicnt = np.zeros(self.numSrcs)



###############################################################
###############################################################

class AdaptiveModule(object):
    def __init__(self, inputSrc, featureNos, incsfaParams, rocParams, newExperiment, savedNetworkFile):
        self.inputSrc = inputSrc                                        # Current input signal source 
        self.featureNos = featureNos                                    # Dimensionality of the slow-feature abstraction
        self.incsfaParams = incsfaParams                                # IncSFA parameters
        self.rocParams = rocParams                                      # ROC parameters 
        self.newExperiment = newExperiment                              # new Experiment flag
        self.savedNetworkFile = savedNetworkFile                        # if newExperiment is False, incsfa/roc node files

        self._init_abstractor()
        
    def _init_abstractor(self):
        if self.newExperiment:
            if self.incsfaParams.get('quadExp', False):
                self.incsfaNode = IncSFA2Node(**self.incsfaParams)
            else:
                self.incsfaNode = IncSFANode(**self.incsfaParams)
            self.rocNode = ROCClusteringNode(**self.rocParams)
        else:
            [self.incsfaNode, self.rocNode] = loadfile(self.savedNetworkFile)

        self.node = [self.incsfaNode, self.rocNode]
        self.progError = [0.0]
        self.dProgError = [0.0]
        

    def getProgressError(self):
        return [self.progError, self.dProgError]

    def getEstMap(self):
        return self.rocNode.getEstMap()

    def _discretize(self, value, minv, maxv, nd):
        states = np.linspace(minv, maxv, nd).astype('int') # discretize states
        return [np.argmin(np.abs(states - value)), states[np.argmin(np.abs(states - value))]] #discrete state value

    def getObservations(self, tau=1):
        #Carry out tau number of interactions
        inputData = np.zeros([tau, self.incsfaParams['input_dim']])
        configData = np.zeros([tau, self.inputSrc.stateLims.shape[0]])
        for i in xrange(tau): 
            inputData[i], configData[i] = self.inputSrc.getObservation()
            self.inputSrc.performAction()
        
        # Get activated ROC-node indices
        rocClusIndx = configData.copy()
        for i in xrange(rocClusIndx.shape[0]):
            for j in xrange(rocClusIndx.shape[1]):
                rocClusIndx[i,j] = self._discretize(configData[i,j],self.inputSrc.stateLims[j,0],self.inputSrc.stateLims[j,1],self.rocNode.dim[j])[0]
 
        return inputData, configData, rocClusIndx 

    def updateAdaptiveModule(self, inputData, rocClusIndx):
        self.progError = []
        self.dProgError = []
        
        #Update IncSFA
        for i in xrange(inputData.shape[0]):
            self.incsfaNode.update(inputData[i:i+1])
        incSFAOutput = self.incsfaNode.execute(inputData)
        self.incsfaNode.monitorVariables()
          
        #Update ROC
        for i in xrange(rocClusIndx.shape[0]):
            self.rocNode.update(incSFAOutput[i,self.featureNos],rocClusIndx[i]) 
            self.progError.append(self.rocNode.getEstimationError())
            self.dProgError.append(self.incsfaNode.derr)

        return incSFAOutput

    def createNewModule(self):
        self._init_abstractor()


###############################################################
###############################################################


class GatingSystem(object):
    def __init__(self, gatingThreshold = 0.3, modulesDir = None):
        self.gatingThreshold = gatingThreshold
        self.modulesDir = modulesDir
        self.trainedModules = []
        self.obsmax = -np.inf
        
    def saveCurrModule(self,module):
        # module contains [incSFANode, ROCNode]
        print 'saving Module'
        assert(len(module)==2)
        self.trainedModules.append(copy.deepcopy(module))
#            savefile(self.modulesDir + '/trainedModule' + str(len(self.trainedModules)+1), module)
        
    def loadModules(self, n=[1]):
        for i in xrange(len(n)):
            module = loadfile(self.modulesDir + '/trainedModule' + str(n[i]))
        self.trainedModules.append(module)

    def genGatingSignal(self, inputData, rocClusIndx):
        encodedOutput = []
        ROCError = []
        
        if len(self.trainedModules) == 0:
            #No trained modules. Gating signal = 1, encoded output = None
            return [1, None]
        else:
            #Get ROC errors for the input 
            for i in xrange(len(self.trainedModules)):
                encodedOutput.append(self.trainedModules[i][0].execute(inputData))
                ROCError.append((self.trainedModules[i][1].getEstimationError(encodedOutput[i],rocClusIndx)))
    
            #Check if any modules already encodes the input 
            if np.min(ROCError) < self.gatingThreshold*len(inputData):
                #Previously encoded input. Gating signal = 0, encoded output = trained insfa-out
                return [0, encodedOutput[np.argmin(ROCError)]]
            else:
                #novel input. Gating signal = 1, encoded output = None
                return [1, None]


###############################################################
###############################################################

class RewardModel(object):
    ThrRew = 0
    NormRew = 1
        
    def __init__(self, numStates, N):
        self.numStates = numStates
        self.N = N
        self.alpha = 2.0/(self.N+1)

        self.rewM = np.zeros([self.numStates,2,self.numStates])
        self.rewMNorm = np.zeros([self.numStates,2,self.numStates])
        self.rewTh = np.zeros([self.numStates])

        if self.NormRew:
            self.R1 = self.rewMNorm
        elif self.ThrRew:
            self.R1 = self.rewTh
            
       
    def update(self, x):
        [s,a,s_,r] = x
        self.rewM[s,a,s_] = self.alpha*r + (1-self.alpha)*self.rewM[s,a,s_]
        
        if self.NormRew:
            if np.max(self.rewM) != 0:
                self.rewMNorm = self.rewM/norm(self.rewM)
            else:
                self.rewMNorm = self.rewM
                
        if self.ThrRew:
            self.rewTh = np.zeros([self.numStates])
            ig = np.argmax(np.diag(self.rewM[:,0,:]))
            self.rewTh[ig] = 1.0
                
        if self.NormRew:
            self.R1 = self.rewMNorm
        elif self.ThrRew:
            self.R1 = self.rewTh

    def R(self, s, a, s_):
        if self.NormRew:
            return self.R1[s,a,s_]
        elif self.ThrRew:
            return self.R1[s_]

    def Rt(self, s, a, s_):
        if self.NormRew:
            if s_ == s:
                return self.R1[s,0,s_]
            else: 
                return self.R1[s,1,s_]
        elif self.ThrRew:
            return self.R1[s_]
        

    def reset(self):
        self.__init__(self.numStates, self.N)



###############################################################
###############################################################



