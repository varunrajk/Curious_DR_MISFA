

import numpy as np
from misc import omegaNode
import types

totStates = 500                         # S^dagger
t = np.linspace(0, 2 * np.pi, 500)

class randomConfig:
    T = totStates
    @classmethod
    def _sig(cls):
        return np.random.randn(cls.T,2)
    signal = _sig
 
     
class emptyConfig:
    T = totStates
    @classmethod
    def _sig(cls):
        return np.NAN*np.ones([cls.T,2])
    signal = _sig

d1 = np.vstack((np.sin(4*t-np.pi/4.)-np.cos(44*t)**2, np.cos(44*t))).T          # Signal 1
d2 = np.vstack((np.sin(3*t)+np.power(np.cos(27*t),2), np.cos(27*t))).T          # Signal 2
d3 = np.vstack((np.cos(12*t), np.cos(2*t)+np.power(np.cos(12*t),2))).T          # Signal 3
X = [d1,d2,d3]

# Signal suboptimality conditions check
onode = omegaNode(eta=0.05,degree=2)
print 'Signal Omegas: ', onode.getOmega(X)
onode.signalOmegaCheck(X, gamma=0.99)


def _sig(cls):
    return X[cls.id]
signalSrcs = []
for i in xrange(len(X)):
    signalSrcs.append(type('signal%dConfig'%(i+1), (object,), {'T':totStates, 'id':i}))
    signalSrcs[i].signal = types.MethodType(_sig, signalSrcs[i])

 
GRIDLIMS = np.array([[0,len(X)-1]])
STIMLOC = np.arange(len(X))
numSrcs = np.product([GRIDLIMS[i,1]-GRIDLIMS[i,0]+1 for i in xrange(len(GRIDLIMS))])
signals = [emptyConfig for _ in xrange(numSrcs)]
 
for i in xrange(len(STIMLOC)):
    signals[STIMLOC[i]] = signalSrcs[i] 

